const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
var database = require("../db/database");

var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const JWT_SECTRT = "carkey";

router.post("/create_new_car", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'new_car'`);
    if (rows_table.length <= 0) {
      const [rows] = await database.query(`CREATE TABLE new_car (
              id INT PRIMARY KEY AUTO_INCREMENT,
              vehicle_model VARCHAR(50) NOT NULL,
              vehicle_number VARCHAR(50) NOT NULL,
              rent_per_day INT NOT NULL,
              seating_capacity INT NOT NULL,
              UNIQUE (vehicle_number)
              )`);
    }
    let car = await database.query(
      `SELECT * FROM new_car WHERE vehicle_number='${req.body.vehicle_number}'`
    );
    console.log(car[0], "car");
    if (car[0].length > 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry a car with this number or model already exists",
      });
    }
    const [rows1] = await database.query(
      `INSERT INTO new_car (vehicle_model, vehicle_number, rent_per_day, seating_capacity) VALUES ('${data.vehicle_model}', '${data.vehicle_number}', '${data.rent_per_day}', '${data.seating_capacity}')`
    );
    console.log(rows1, `Table exists.`);
    res.status(200).json({
      error: false,
      message: "Car Data Insertded Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/buy_car", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");
    if (data.date.length == 0 && data.no_of_days <= 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry Please fill all field",
      });
    }
    const decodedToken = jwt.verify(req.body.userIdToken, JWT_SECTRT);
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'buy_car'`);
    if (rows_table.length <= 0) {
      const [rows] = await database.query(`CREATE TABLE buy_car (
              id INT PRIMARY KEY AUTO_INCREMENT,
              userId VARCHAR(50) NOT NULL,
              car_id INT NOT NULL,
              date VARCHAR(50) NOT NULL,
              no_of_days INT NOT NULL
              )`);
    }
    let car = await database.query(
      `SELECT * FROM buy_car WHERE car_id='${req.body.car_id}'`
    );
    console.log(decodedToken, car[0], "car");
    if (car[0].length > 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry a car with this number or model already buy",
      });
    }
    const [rows1] = await database.query(
      `INSERT INTO buy_car (car_id, userId, date, no_of_days) VALUES ('${data.car_id}', '${decodedToken.id}', '${data.date}', '${data.no_of_days}')`
    );
    res
      .status(200)
      .json({ error: false, message: "Car Buy Successfully", data: rows1 });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/delete_car", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");

    const [rows1] = await database.query(
      `DELETE FROM new_car WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "Car Data Deleted Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/edit_car", async (req, res) => {
  try {
    const data = req.body;
    console.log(data, "data");

    const [rows] = await database.query(
      `SELECT * FROM new_car
        WHERE id = ${data.id};`
    );
    let carDetail = rows[0];
    console.log(rows, carDetail, "carDetail");
    const [rows1] = await database.query(
      `UPDATE new_car
      SET 
      vehicle_model =  '${
        data.vehicle_model ? data.vehicle_model : carDetail.vehicle_model
      }',
      vehicle_number = '${
        data.vehicle_number ? data.vehicle_number : carDetail.vehicle_number
      }',
      rent_per_day =   '${
        data.rent_per_day ? data.rent_per_day : carDetail.rent_per_day
      }',
      seating_capacity = '${
        data.seating_capacity
          ? data.seating_capacity
          : carDetail.seating_capacity
      }'
      WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "Car Data Deleted Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.get("/get_all_car", async (req, res) => {
  try {
    let allCar = await database.query(`SELECT * FROM new_car`);
    // console.log(allCar, `Table exists.`);
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.get("/get_rent_car", async (req, res) => {
  try {
    let allCar = await database.query(`SELECT * FROM buy_car`);
    console.log(allCar[0], `allCar[0].`);
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_user_rent_car", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.userIdToken, JWT_SECTRT);
    let allCar = await database.query(
      `SELECT * FROM buy_car WHERE userId='${decodedToken.id}'`
    );
    console.log(allCar, `Table exists.`);
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

module.exports = router;
