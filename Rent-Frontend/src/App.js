import { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Index from "./components/Index";
import Register from "./components/Register";
import Navbar from "./components/Navbar";
import Login from "./components/Login";
import AdminHome from "./components/AdminHome";
import UserHome from "./components/UserHome";
import AdminSignup from "./components/AdminSignup";
import AllBuyCar from "./components/AllBuyCar";
import UserBuyCar from "./components/UserBuyCar";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";

function App() {
  return (
    <BrowserRouter>
      <ToastContainer />
      <Navbar />
      <Routes>
        <Route path="/" element={<Index />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/adminsignup" element={<AdminSignup />}></Route>
        <Route path="/adminhome" element={<AdminHome />}></Route>
        <Route path="/userhome" element={<UserHome />}></Route>
        <Route path="/allbuycar" element={<AllBuyCar />}></Route>
        <Route path="/userbuycar" element={<UserBuyCar />}></Route>
        {/* <Route path="/bat" element={<Bat clLink={clLink} />}></Route> */}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
