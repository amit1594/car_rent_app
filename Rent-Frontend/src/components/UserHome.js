import React, { useEffect, useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { showToast } from "../common/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_ALL_CAR = `${API_URL}/api/adminController/get_all_car`;
export const BUY_CAR = `${API_URL}/api/adminController/buy_car`;

const UserHome = () => {
  let navigate = useNavigate();
  const [allCar, setAllCar] = useState([]);
  const [no_of_days, setNo_of_days] = useState(0);
  const [date, setDate] = useState("");
  const [car_id, setCar_id] = useState(0);
  const fetchCar = async () => {
    try {
      const response = await fetch(GET_ALL_CAR, {
        method: "GET",
      });
      const json = await response.json();
      setAllCar(json.data);
      console.log(json.data, "json");
    } catch (error) {
      console.error(error);
    }
  };

  const useRefId = useRef();
  const editCar = async (v) => {
    useRefId.current = v;
    console.log(v, "v1");
  };

  const buyCar = async () => {
    try {
      const response = await fetch(BUY_CAR, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          car_id: car_id,
          userIdToken: localStorage.getItem("token"),
          no_of_days: no_of_days,
          date: date,
        }),
      });
      const json = await response.json();
      console.log(json, "json");
      if (!json.error) {
        showToast(json.message, "success");
      } else {
        showToast(json.message, "error");
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchCar();
  }, []);
  return (
    <>
      <h3 className="text-center my-2">Home Page</h3>
      <div
        className="card my-3"
        style={{
          height: "550px",
          width: "1200px",
          marginLeft: "100px",
          marginRight: "110px",
        }}
      >
        <div className="card-body">
          {localStorage.getItem("token") && (
            <div style={{ marginLeft: "900px" }}>
              <button className="btn btn-success">
                <Link style={{ color: "white" }} to="/userbuycar">
                  My Rent Car
                </Link>
              </button>
            </div>
          )}
          <div className="my-3">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Vehicle model</th>
                  <th scope="col">Vehicle number</th>
                  <th scope="col">seating capacity</th>
                  <th scope="col">rent per day</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                {!allCar && <p className="text-center my-5">No Record Found</p>}
                {allCar &&
                  allCar.map((item) => (
                    <tr>
                      <td>{item.id}</td>
                      <td>{item.vehicle_model}</td>
                      <td>{item.vehicle_number}</td>
                      <td>{item.seating_capacity}</td>
                      <td>{item.rent_per_day}</td>
                      {localStorage.getItem("token") ? (
                        <div className="btn-group">
                          <button
                            type="button"
                            className="btn btn-secondary btn-sm dropdown-toggle my-2"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            Action
                          </button>
                          <div className="dropdown-menu">
                            <a
                              onClick={() => setCar_id(item.id)}
                              className="dropdown-item"
                              data-toggle="modal"
                              data-target="#exampleModalUpdate"
                              href="#"
                            >
                              Buy
                            </a>
                          </div>
                        </div>
                      ) : (
                        <Link to="/login">Logged in</Link>
                      )}
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="exampleModalUpdate"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Modal title
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label for="formGroupExampleInput">No. of Days</label>
                  <input
                    type="number"
                    className="form-control"
                    id="no_of_days"
                    name="no_of_days"
                    onChange={(e) => {
                      setNo_of_days(e.target.value);
                    }}
                    placeholder="Enter Days"
                  />
                </div>
                <div className="form-group">
                  <label for="formGroupExampleInput2">Date</label>
                  <input
                    type="text"
                    className="form-control"
                    id="date"
                    name="date"
                    onChange={(e) => {
                      setDate(e.target.value);
                    }}
                    placeholder="DD/MM/YYYY"
                  />
                </div>
              </form>
              <div className="text-center">
                <button
                  type="submit"
                  onClick={buyCar}
                  data-dismiss="modal"
                  aria-label="Close"
                  className="btn btn-success"
                >
                  Rent
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserHome;
