import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Link, useNavigate } from "react-router-dom";
import { showToast } from "../common/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const CREATE_NEW_CAR = `${API_URL}/api/adminController/create_new_car`;

const initialValues = {
  vehicle_model: "",
  vehicle_number: "",
  rent_per_day: "",
  seating_capacity: "",
};

const carSchema = Yup.object().shape({
  vehicle_model: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("vehicle_model is required"),
  vehicle_number: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("vehicle_number is required"),
  rent_per_day: Yup.number().required("rent_per_day is required"),
  seating_capacity: Yup.number().required("seating_capacity is required"),
});

const AddNewCar = () => {
  let navigate = useNavigate();
  const formik = useFormik({
    initialValues,
    validationSchema: carSchema,
    onSubmit: async (values) => {
      console.log(values, typeof values.rent_per_day, "values");
      try {
        const response = await fetch(CREATE_NEW_CAR, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            vehicle_model: values.vehicle_model,
            vehicle_number: values.vehicle_number,
            seating_capacity: values.seating_capacity,
            rent_per_day: values.rent_per_day,
          }),
        });
        const json = await response.json();
        console.log(json, "json");
        if (!json.error) {
          showToast(json.message, "success");
          window.location.reload();
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  useEffect(() => {}, []);
  return (
    <>
      <button
        type="button"
        className="btn btn-primary float-right"
        style={{ marginBottom: "15px" }}
        data-toggle="modal"
        data-target="#exampleModal"
      >
        Add New Car
      </button>

      <div
        className="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Add New Car
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form
                onSubmit={formik.handleSubmit}
                id="kt_create_car_submit"
                noValidate
              >
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("vehicle_model")}
                    id="vehicle_model"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Vehicle model
                  </label>
                  {formik.touched.vehicle_model &&
                    formik.errors.vehicle_model && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert">
                            {formik.errors.vehicle_model}
                          </span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("vehicle_number")}
                    id="vehicle_number"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Vehicle number
                  </label>
                  {formik.touched.vehicle_number &&
                    formik.errors.vehicle_number && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert">
                            {formik.errors.vehicle_number}
                          </span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="number"
                    {...formik.getFieldProps("seating_capacity")}
                    id="seating_capacity"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    seating capacity
                  </label>
                  {formik.touched.seating_capacity &&
                    formik.errors.seating_capacity && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert">
                            {formik.errors.seating_capacity}
                          </span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="number"
                    {...formik.getFieldProps("rent_per_day")}
                    id="rent_per_day"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    rent per day
                  </label>
                  {formik.touched.rent_per_day &&
                    formik.errors.rent_per_day && (
                      <div className="fv-plugins-message-container">
                        <div className="fv-help-block">
                          <span role="alert">{formik.errors.rent_per_day}</span>
                        </div>
                      </div>
                    )}
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="submit"
                    id="kt_create_car_submit"
                    className="btn btn-success gradient-custom-4 text-body"
                  >
                    Create Car
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      {localStorage.getItem("token") && (
            <div>
              <button className="btn btn-success">
                <Link style={{ color: "white" }} to="/allbuycar">
                  All Rent Car
                </Link>
              </button>
            </div>
          )}
    </>
  );
};

export default AddNewCar;
