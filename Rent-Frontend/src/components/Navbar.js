import React, { useEffect, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_ID = `${API_URL}/api/auth/get_id_by_token`;

const Navbar = () => {
  const isAdmin = useRef();
  let navigate = useNavigate();
  const logout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  useEffect(() => {
    const getAdmit = async () => {
      try {
        const response = await fetch(GET_ID, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            authtoken: localStorage.getItem("token"),
          }),
        });
        const json = await response.json();
        console.log(json.isAdmin, "token");
        if (!json.error) {
          isAdmin.current = json.isAdmin;
        } else {
          isAdmin.current = -1;
        }
      } catch (error) {
        console.error(error);
      }
    };
    getAdmit();
    setTimeout(() => {
      // setMessage("This message appeared after a delay!");
    }, 2000);
  }, []);

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        {/* {isAdmin.current && <Link className="navbar-brand mx-5" to="/adminhome">Home</Link>}
  {!isAdmin.current && <Link className="navbar-brand mx-5" to="/UserHome">Home</Link>} */}
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarText"
          aria-controls="navbarText"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              {/* {console.log(isAdmin.current, "isAdmin.current")} */}
              {/* {isAdmin.current && <Link className="nav-link" to="/allbuycar">Rent Car<span className="sr-only">(current)</span></Link>}
        {isAdmin.current==-1 && <Link className="nav-link" to="/userbuycar">My Rent Car<span className="sr-only">(current)</span></Link>} */}
            </li>
          </ul>
          {localStorage.getItem("token") && (
            <span
              onClick={logout}
              className="navbar-text mx-5"
              style={{ cursor: "pointer" }}
            >
              Logout
            </span>
          )}
          {!localStorage.getItem("token") && (
            <span
              onClick={logout}
              className="navbar-text mx-5"
              style={{ cursor: "pointer" }}
            >
              <Link to="/login">Login</Link>
            </span>
          )}
        </div>
      </nav>
    </>
  );
};

export default Navbar;
