import React, { useEffect, useState, useRef } from "react";
import AddNewCar from "./AddNewCar";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { showToast } from "../common/toastify";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_ALL_CAR = `${API_URL}/api/adminController/get_all_car`;
export const DELETE_CAR = `${API_URL}/api/adminController/delete_car`;
export const EDIT_CAR = `${API_URL}/api/adminController/edit_car`;

const carSchema = Yup.object().shape({
  vehicle_model: Yup.string(),
  vehicle_number: Yup.string(),
  rent_per_day: Yup.number(),
  seating_capacity: Yup.number(),
});

const AdminHome = () => {
  let navigate = useNavigate();
  const [allCar, setAllCar] = useState([]);
  const fetchCar = async () => {
    try {
      const response = await fetch(GET_ALL_CAR, {
        method: "GET",
      });
      const json = await response.json();
      setAllCar(json.data);
      console.log(json.data, "json");
    } catch (error) {
      console.error(error);
    }
  };

  const deleteCar = async (id) => {
    try {
      const response = await fetch(DELETE_CAR, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      });
      const json = await response.json();
      console.log(json, "json");
      if (!json.error) {
        window.location.reload();
        showToast(json.message, "success");
      } else {
        showToast(json.message, "error");
        //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const initialValues = {
    vehicle_model: "",
    vehicle_number: "",
    rent_per_day: "",
    seating_capacity: "",
  };
  const useRefId = useRef();
  const editCar = async (v) => {
    useRefId.current = v;
    console.log(v, "v1");
  };
  const formik = useFormik({
    initialValues,
    validationSchema: carSchema,
    onSubmit: async (values) => {
      console.log(useRefId.current, values, "values");
      try {
        const response = await fetch(EDIT_CAR, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: useRefId.current,
            vehicle_model: values.vehicle_model,
            vehicle_number: values.vehicle_number,
            seating_capacity: values.seating_capacity,
            rent_per_day: values.rent_per_day,
          }),
        });
        const json = await response.json();
        console.log(json, "json");
        if (!json.error) {
          window.location.reload();
          showToast(json.message, "success");
          navigate("/adminhome");
        } else {
          showToast(json.message, "error");
          //   showToast(`Ticket Creation Failed, ${json.error.message}`, "error");
        }
      } catch (error) {
        console.error(error);
      }
    },
  });

  useEffect(() => {
    fetchCar();
  }, []);
  return (
    <>
      <h3 className="text-center my-2">Admin Home Page</h3>
      <div
        className="card my-3"
        style={{
          height: "550px",
          width: "1200px",
          marginLeft: "100px",
          marginRight: "110px",
        }}
      >
        <div className="card-body">
          <div style={{ marginLeft: "750px" }}>
            <AddNewCar />
          </div>
          <div className="my-3">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Vehicle model</th>
                  <th scope="col">Vehicle number</th>
                  <th scope="col">seating capacity</th>
                  <th scope="col">rent per day</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                {!allCar && <p className="text-center my-5">No Record Found</p>}
                {allCar &&
                  allCar.map((item) => (
                    <tr>
                      <td>{item.id}</td>
                      <td>{item.vehicle_model}</td>
                      <td>{item.vehicle_number}</td>
                      <td>{item.seating_capacity}</td>
                      <td>{item.rent_per_day}</td>
                      <div className="btn-group">
                        <button
                          type="button"
                          className="btn btn-secondary btn-sm dropdown-toggle my-2"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Action
                        </button>
                        <div className="dropdown-menu">
                          <a
                            onClick={() => editCar(item.id)}
                            className="dropdown-item"
                            data-toggle="modal"
                            data-target="#exampleModalUpdate"
                            href="#"
                          >
                            edit
                          </a>
                          <a
                            className="dropdown-item"
                            onClick={() => deleteCar(item.id)}
                            href="#"
                          >
                            delete
                          </a>
                        </div>
                      </div>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="exampleModalUpdate"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Modal title
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form
                onSubmit={formik.handleSubmit}
                id="kt_update_car_submit"
                noValidate
              >
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("vehicle_model")}
                    id="vehicle_model"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Vehicle model
                  </label>
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="text"
                    {...formik.getFieldProps("vehicle_number")}
                    id="vehicle_number"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    Vehicle number
                  </label>
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="number"
                    {...formik.getFieldProps("seating_capacity")}
                    id="seating_capacity"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    seating capacity
                  </label>
                </div>
                <div className="form-outline mb-4">
                  <input
                    type="number"
                    {...formik.getFieldProps("rent_per_day")}
                    id="rent_per_day"
                    className="form-control form-control-lg"
                  />
                  <label className="form-label" for="form3Example1cg">
                    rent per day
                  </label>
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="submit"
                    id="kt_update_car_submit"
                    className="btn btn-success gradient-custom-4 text-body"
                  >
                    Update Car
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminHome;
