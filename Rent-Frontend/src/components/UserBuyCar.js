import React, { useEffect, useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
const API_URL = process.env.REACT_APP_API_URL;
export const GET_USER_RENT_CAR = `${API_URL}/api/adminController/get_user_rent_car`;

const AllBuyCar = () => {
  const [allCar, setAllCar] = useState([]);
  const fetchCar = async () => {
    try {
      const response = await fetch(GET_USER_RENT_CAR, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          userIdToken: localStorage.getItem("token"),
        }),
      });
      const json = await response.json();
      setAllCar(json.data);
      console.log(json.data, "json");
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchCar();
  }, []);
  return (
    <>
      <h3 className="text-center my-2">User Rent Car</h3>
      <div
        className="card my-3"
        style={{
          height: "550px",
          width: "1200px",
          marginLeft: "100px",
          marginRight: "110px",
        }}
      >
        <div className="card-body">
          <div className="my-3">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Car Id</th>
                  <th scope="col">No Of Days</th>
                  <th scope="col">Starting Date</th>
                </tr>
              </thead>
              <tbody>
                {!allCar && <p className="text-center my-5">No Record Found</p>}
                {allCar &&
                  allCar.map((item) => (
                    <tr>
                      <td>{item.id}</td>
                      <td>{item.car_id}</td>
                      <td>{item.no_of_days}</td>
                      <td>{item.date}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default AllBuyCar;
